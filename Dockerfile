ARG version='latest'
FROM docker.osgeo.org/geoserver:${version}

# specify volume mount points
ENV GEOSERVER_DATA_DIR=/mnt/geoserver_data_dir
ENV GEOWEBCACHE_CACHE_DIR=/mnt/geoserver_caching

# specify extensions to install and location
ENV ADDITIONAL_LIBS_DIR=/opt/additional/libs/
ENV STABLE_EXTENSIONS="vectortiles,gdal,wps,printing,libjpeg-turbo,control-flow,pyramid,monitor,inspire,csw,ysld,web-resource"
ENV COMMUNITY_EXTENSIONS=

# specify ssl options
ENV SSL=false
ENV HTTP_PORT=8080
ENV HTTP_PROXY_NAME=
# needed for openshift
ENV HTTP_PROXY_PORT=443 
ENV HTTP_REDIRECT_PORT=
ENV HTTP_CONNECTION_TIMEOUT=20000
ENV HTTP_SCHEME="HTTPS"
ENV HTTPS_PORT=8443
ENV HTTPS_MAX_THREADS=150
ENV HTTPS_CLIENT_AUTH=
ENV HTTPS_PROXY_NAME=
ENV HTTPS_PROXY_PORT=
ENV JKS_FILE=letsencrypt.jks
ENV JKS_KEY_PASSWORD='geoserver'
ENV KEY_ALIAS=letsencrypt
ENV JKS_STORE_PASSWORD='geoserver'
ENV P12_FILE=letsencrypt.p12
ENV PKCS12_PASSWORD='geoserver'
ENV LETSENCRYPT_CERT_DIR=/etc/letsencrypt
ENV CHARACTER_ENCODING='UTF-8'

# install pkg used to edit server.xml with ssl options
RUN apt-get update && apt-get install -y --no-install-recommends xsltproc

# copy scripts and files
COPY scripts/install-extensions.sh /opt/
COPY scripts/startup.sh /opt/
COPY scripts/configure_ssl.sh /opt/
COPY build_data/letsencrypt-tomcat.xsl ${CATALINA_HOME}/conf/ssl-tomcat.xsl

# install geoserver extensions
RUN bash /opt/install-extensions.sh
RUN rm /opt/install-extensions.sh

# edit permissions for openshift user (user with random uid and gid 0)
RUN chmod g=u /etc/passwd && mkdir -p /home/geoserveruser
RUN chgrp -R 0 /opt ${ADDITIONAL_LIBS_DIR} /home/geoserveruser
RUN chmod -R g=u /opt ${ADDITIONAL_LIBS_DIR} /home/geoserveruser
RUN chmod g+x /opt/startup.sh

ENTRYPOINT ["/opt/startup.sh"]