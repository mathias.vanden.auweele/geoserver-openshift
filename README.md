# Geoserver docker image

## Building

Put desired version number in .gitlab-ci.yml and push


### Acceptance

Push to master

### Production

Add tag with geoserver version


### Precaution

Create a snapshot of pvc geoserver.datadir before updating
